import cv2
import csv
import pandas as pd 
from PIL import Image
from datetime import date

participants = pd.read_csv("ranklist.csv")

output_directory_path = "generated_certificates"

font_size = 1
font_color = (0,0,0)

#uploading the certificate data 
ch= input("\nwant to upload a new certificate (y/n):")
if ch=='y' or ch=='Y':
    certiName = input("Enter the student Name:")
    Class  = input("Enter the student Grade(I,II,..) :")
    Position = input("Enter the student Position:")
    Sport = input("Enter the Acheived at:")
    text = [certiName, Class, Position, Sport]
    with open('ranklist.csv','a') as f:
        Csvwriter = csv.writer(f)
        Csvwriter.writerow(text)

# Generating the certificates
template_file_path = "certificate.png"

today = date.today()

# dd/mm/YY
d1 = today.strftime("%d/%m/%Y")

for index, row in participants.iterrows():

    certiName = row["Name"].title()
    Class = row["Class"].title()
    Position= row["Position"].title()
    Sport = row["Sport"].title()
    img = cv2.imread(template_file_path)
    font = cv2.FONT_HERSHEY_SCRIPT_COMPLEX
    font2 = cv2.FONT_HERSHEY_COMPLEX_SMALL

    text = [certiName, Class, Position, Sport]
      
    for i in range(len(text)):
        if i== 0:
            textsize = cv2.getTextSize(text[i],font,0.5,2)
            cv2.putText(img, text[i], (320,290), font, font_size, font_color, 1)
        elif i==1:
            textsize = cv2.getTextSize(text[i],1,0.5,2)
            cv2.putText(img, text[i], (180,330), font2, font_size, font_color, 1)
        elif i==2:
            textsize = cv2.getTextSize(text[i],1,0.5,2)
            cv2.putText(img, text[i], (360,330), font2, font_size, font_color, 1)
        else:
            textsize = cv2.getTextSize(text[i],1,0.5,2)
            cv2.putText(img, text[i], (550,330), font2, font_size, font_color, 1)
        
        textsize = cv2.getTextSize(d1,1,0.5,2)
        cv2.putText(img, d1, (350,420), font2, font_size, font_color,1)
        cv2.putText(img, "orchids", (550,420), font, font_size, font_color,1)


    certiPath = "{}/{}.png".format(output_directory_path,row["Name"])
    cv2.imwrite(certiPath, img)

# Accessing the certificate
c = input("\nDo you want the certificate (y/n): ")
if c=='y' or c =='Y' :
    flag = 0
    certiName = input("Enter the student Name:")
    Class  = input("Enter the student Grade(I,II,..) :")
    for index, row in participants.iterrows():

        if certiName in row["Name"] and Class in row["Class"] :
            path =  output_directory_path+'/'+certiName+'.png'
            img = cv2.imread(path)
            cv2.imshow('image',img)
            flag = 1

    if flag== 0 :
        print("No certificate found!!!")
        
cv2.waitKey(0) 
cv2.destroyAllWindows()