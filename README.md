# e-certificate
E-certificate is the process of generating the certificate by giving some basic data as a input.
In this I had used python language for generating the certificate.

Requirements:
 - install python
 - Any editor(vs code or pycharam or..)

install following:
 -install python
 -install pip
 -install pandas
 -install cv2 or opencv

process:
 - open editor and save the code.py, certificate.png, ranklist.csv and generated_certificate folder in a single folder
 - install all the softwares which are mentioned above
 - create the virtual environment
     virtualenv env 
 - activate env
     source env/bin/activate
 - move to the folder where you have saved the files
 - run the code.py files
     python code.py
 